package ru.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.test.config.ConfigParser;
import ru.test.model.DBConfig;
import ru.test.parsers.DOMparserXML;
import ru.test.parsers.ParserXmlXpath;
import ru.test.parsers.Xml1toXml2throughXslt;
import ru.test.servise.DBWorkerServise;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Start {

    private static Logger logger = LoggerFactory.getLogger(Start.class);

    public static void main(String[] args) {

        logger.debug("Start!");

        DBConfig dbConfig = new DBConfig();


        Properties props = new Properties();
        try {
            props.load(new FileInputStream("src/main/resources/configDb.ini"));
        } catch (IOException e) {
            logger.error("File 'configDb.ini' not found: ", e);
        }

        dbConfig.setUrl(props.getProperty("DATABASE_URL"));
        dbConfig.setDriver(props.getProperty("DATABASE_DRIVER"));
        dbConfig.setCountN(1000000);
        logger.debug("Main class DBConfig created.");

        DBWorkerServise dbWorkerServise = new DBWorkerServise(dbConfig);
        logger.debug("Unloading in DB completed.");


        ////
        ConfigParser configParser = null;
        try {
            configParser = new ConfigParser("src/main/resources/configParsers.ini");
        } catch (IOException e) {
            logger.error("File 'configParsers.ini' not found: ",e);
        }
        ///

        DOMparserXML doMparserXML = new DOMparserXML(configParser.getPathXml1());
        doMparserXML.writer(dbWorkerServise.getAll());
        logger.debug("1.xml created!");

        Xml1toXml2throughXslt xml2 = new Xml1toXml2throughXslt(configParser.getPathXml1(),configParser.getPathXml2(),configParser.getPathXsl());
        xml2.convertXml();
        logger.debug("2.xml created!");

        System.out.println(ParserXmlXpath.parseSumField(configParser.getPathXml2()));
        logger.debug("The amount is displayed on the screen.");

        logger.debug("Finish!");
    }


}
