package ru.test.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigParser {

    ///////////////////////////////////////////////

    private String pathXml1;
    private String pathXml2;
    private String pathXsl;
    private Logger logger = LoggerFactory.getLogger(ConfigParser.class);

    ///////////////////////////////////////////////

    public ConfigParser(String fileName) throws IOException{loadProps(new File(fileName));}

    ///////////////////////////////////////////////

    protected void loadProps(File file)throws IOException{
        try
        {
            Properties props = new Properties();
            props.load(new FileInputStream(file));
            pathXml1 = props.getProperty("PARSER_1.XML");
            pathXml2 = props.getProperty("PARSER_2.XML");
            pathXsl = props.getProperty("PARSER_XSL");

        }
        catch (IOException e) {
            logger.error("XML config reading error: ", e);
            throw e;
        }
    }

    public String getPathXml1() {
        return pathXml1;
    }

    public String getPathXml2() {
        return pathXml2;
    }

    public String getPathXsl() {
        return pathXsl;
    }

    public Logger getLogger() {
        return logger;
    }
}
