package ru.test.config;

public class DataSourceInitializeException extends Exception  {
    public DataSourceInitializeException() { super(); }
    public DataSourceInitializeException(String msg) { super(msg); }
    public DataSourceInitializeException(Throwable throwable) { super(throwable); }
    public DataSourceInitializeException(String msg, Throwable throwable) { super(msg, throwable); }
}
