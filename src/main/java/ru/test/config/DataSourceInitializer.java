package ru.test.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sqlite.SQLiteDataSource;
import ru.test.model.DBConfig;

import javax.sql.DataSource;

public class DataSourceInitializer {

    ////////////////////////////////////////////////

    private DBConfig dbConfig;

    ////////////////////////////////////////////////

    public DataSourceInitializer(DBConfig dbConfig) {
        this.dbConfig = dbConfig;
    }

    ////////////////////////////////////////////////

    public DataSource getDataSource() throws DataSourceInitializeException {

        DataSource dataSource = null;
        String driver = dbConfig.getDriver();

        if(driver.contains("mysql"))
            dataSource = getMySqlDataSource();

        if(driver.contains("sqlite"))
            dataSource = getSQLiteDataSource();

        if(dataSource == null)
            throw new DataSourceInitializeException("Unsupported JDBC driver: " + driver);

        return dataSource;
    }

    public DataSource getMySqlDataSource() {
        //РАсширяем возможности программы. Теперь можно использовать базу Mysql
        //Также, можно добовлять и другие драйвера
        return null;
    }

    public DataSource getSQLiteDataSource() {
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl(dbConfig.getUrl());
        return dataSource;
    }
}
