package ru.test.model;

import java.io.Serializable;

/**
 * Created by Ziaks on 08.10.2017.
 */
public class DBConfig implements Serializable{

    //////////////////////////////////////////////////////////////////

    private String url;
    private int countN;
    private String driver;

    /////////////////////////////////////////////////////////////////

    public DBConfig() {}

    ////////////////////////////////////////////////////////////////

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getCountN() {
        return countN;
    }

    public void setCountN(int countN) {
        this.countN = countN;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DBConfig dbConfig = (DBConfig) o;

        if (countN != dbConfig.countN) return false;
        if (!url.equals(dbConfig.url)) return false;
        return driver.equals(dbConfig.driver);
    }

    @Override
    public int hashCode() {
        int result = url.hashCode();
        result = 31 * result + countN;
        result = 31 * result + driver.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "DBConfig{" +
                "url='" + url + '\'' +
                ", countN=" + countN +
                ", driver='" + driver + '\'' +
                '}';
    }
}
