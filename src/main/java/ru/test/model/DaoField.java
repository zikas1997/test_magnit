package ru.test.model;

public class DaoField {

    //////////////////////////////////////////

    private int number;

    /////////////////////////////////////////

    public DaoField(int number) {
        this.number = number;
    }
    public DaoField(){}
    /////////////////////////////////////////

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
