package ru.test.parsers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.test.model.DaoField;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DOMparserXML {

    //////////////////////////////////////////////////

    private String fileName;
    private Transformer transformer;
    private DOMSource source;
    private Document document;
    private static Logger logger = LoggerFactory.getLogger(DOMparserXML.class);

    /////////////////////////////////////////////////

    public DOMparserXML(String fileName){
        this.fileName = fileName;
        try {
           document  = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
           transformer = TransformerFactory.newInstance().newTransformer();
        } catch (ParserConfigurationException e) {
            logger.error("Error in parser configuration.",e);
        } catch (TransformerConfigurationException e) {
            logger.error("Error in transformer configuration.",e);
        }

    }
    public DOMparserXML(){}

    ////////////////////////////////////////////////

    public void writer(Collection col){

        List<DaoField> collection = new ArrayList();
        collection.addAll(col);


        Element entries = document.createElement("entries");
        document.appendChild(entries);

        for(DaoField n : collection) {

            Element entry = document.createElement("entry");
            Element field = document.createElement("field");
            field.setTextContent(String.valueOf(n.getNumber()));
            entry.appendChild(field);
            entries.appendChild(entry);
        }

        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(fileName));
            transformer.transform(source, result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

    public Collection reader(){
        /////////////////
        //Тело программы
        ////////////////

        return null;
    }
}
