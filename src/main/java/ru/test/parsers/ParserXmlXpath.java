package ru.test.parsers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.test.Start;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class ParserXmlXpath {

    private static Logger logger = LoggerFactory.getLogger(ParserXmlXpath.class);

    public static String parseSumField(String fileName)  {
        SumUseHandler userhandler = null;
        try {
            File inputFile = new File(fileName);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            userhandler = new SumUseHandler();
            saxParser.parse(inputFile, userhandler);
        } catch (Exception e) {
            logger.error("Error to SAX parsers. ", e);
        }

        return String.valueOf(userhandler.getSumN());
    }

}
class SumUseHandler extends DefaultHandler{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private long sumN;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("entry")) {
            String rollNo = attributes.getValue("field");
            sumN = sumN + Integer.parseInt( attributes.getValue("field"));
        }

    }

    public long getSumN() {
        return sumN;
    }
}
