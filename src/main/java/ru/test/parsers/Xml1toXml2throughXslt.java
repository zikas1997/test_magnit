package ru.test.parsers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Xml1toXml2throughXslt {

    //////////////////////////////////////////////////////////////////////////////////

    private String inFilename;
    private String outFilename;
    private String xslFilename;
    private static Logger logger = LoggerFactory.getLogger(Xml1toXml2throughXslt.class);

    //////////////////////////////////////////////////////////////////////////////////

    public Xml1toXml2throughXslt(String inFilename, String outFilename, String xslFilename) {
        this.inFilename = inFilename;
        this.outFilename = outFilename;
        this.xslFilename = xslFilename;
    }

    //////////////////////////////////////////////////////////////////////////////////

    public void convertXml(){
        try {

            TransformerFactory factory = TransformerFactory.newInstance();

            Templates template = factory.newTemplates(new StreamSource(
                    new FileInputStream(xslFilename)));

            Transformer xformer = template.newTransformer();

            Source source = new StreamSource(new FileInputStream(inFilename));
            Result result = new StreamResult(new FileOutputStream(outFilename));

            xformer.transform(source, result);
        } catch (FileNotFoundException e) {
            logger.error("File xsl not found: ", e);
        } catch (TransformerConfigurationException e) {
            logger.error("Error in transformer configuration.",e);
        } catch (TransformerException e) {
            logger.error("Error in transformer. ", e);
        }
    }
}
