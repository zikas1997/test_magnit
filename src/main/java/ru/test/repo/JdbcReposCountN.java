package ru.test.repo;

import ru.test.model.DBConfig;
import ru.test.model.DaoField;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class JdbcReposCountN extends JdbcRepository {

    //////////////////////////////////////////////////

    private DBConfig dbConfig;

    //////////////////////////////////////////////////

    public JdbcReposCountN(DBConfig dbConfig, DataSource dataSource) {
        super(dbConfig, dataSource);
        this.dbConfig = dbConfig;

    }

    /////////////////////////////////////////////////

    public void create() throws SQLException {
        Connection connection = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try{
            PreparedStatement ps = connection.prepareStatement("INSERT INTO test(Field) VALUES (?)");
            final int batchSize = 1000;
            int count = 0;
            for (int i = 0; i < dbConfig.getCountN(); i++) {
                ps.setInt(1, i+1);
                ps.addBatch();
                if((++count % batchSize == 0)||(i == dbConfig.getCountN()-1))
                    ps.executeBatch();
                }
            connection.commit();
            ps.executeBatch();
        }catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        }
        finally {
            closeConnection(connection);
        }
    }


    public void delete() {

        Connection connection = null;
        try {
            connection = getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            Statement st = connection.createStatement();
            st.execute("DELETE FROM test");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                closeConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public Collection read() {
        Connection connection = null;
        try {
            connection = getConnection();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        Collection listDaoField = new ArrayList();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM test");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()){
                DaoField daoField = new DaoField();
                daoField.setNumber(resultSet.getInt("Field"));
                listDaoField.add(daoField);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                closeConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return listDaoField;
    }


    public void update(){
        delete();
        try {
            create();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
