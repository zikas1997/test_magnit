package ru.test.repo;

import ru.test.model.DBConfig;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;

/**
 * Created by Ziaks on 08.10.2017.
 */
public abstract class JdbcRepository {

    ////////////////////////////////////////////////////

    private DBConfig DBConfig;
    private DataSource dataSource;

    ////////////////////////////////////////////////////

    public JdbcRepository(DBConfig DBConfig, DataSource dataSource) {
        this.DBConfig = DBConfig;
        this.dataSource = dataSource;
    }

    protected JdbcRepository(DataSource dataSource) {}
    protected JdbcRepository(){}

    ////////////////////////////////////////////////////

    protected Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    ///////////////////////////////////////////////////

    protected void closeConnection(Connection connection) throws SQLException {
        if(connection != null) {
            connection.close();
        }
    }

    ////////////////////////////////////////////////////

    protected void rollback(Connection connection) throws SQLException {
        connection.rollback();
    }

    ////////////////////////////////////////////////////
}
