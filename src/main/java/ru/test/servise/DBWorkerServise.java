package ru.test.servise;

import ru.test.config.DataSourceInitializeException;
import ru.test.config.DataSourceInitializer;
import ru.test.model.DBConfig;
import ru.test.repo.JdbcReposCountN;

import javax.sql.DataSource;
import java.util.Collection;

public class DBWorkerServise {

    /////////////////////////////////////////////

    private DBConfig dbConfig;
    private DataSource dataSource;
    private JdbcReposCountN jdbcReposCountN;

    ////////////////////////////////////////////

    public DBWorkerServise(DBConfig dbConfig)  {
        this.dbConfig = dbConfig;
        DataSourceInitializer dataSourceInitializer = new DataSourceInitializer(dbConfig);
        try {
            dataSource = dataSourceInitializer.getDataSource();
        } catch (DataSourceInitializeException e) {
            e.printStackTrace();
        }
        jdbcReposCountN = new JdbcReposCountN(dbConfig,dataSource);
        create();
    }
    ///////////////////////////////////////////

    public Collection getAll(){
        return  jdbcReposCountN.read();
    }

    public void create() {
        jdbcReposCountN.update();
    }
}
